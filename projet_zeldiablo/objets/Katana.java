package objets;

import zeldiablo.Aventurier;
import zeldiablo.Labyrinthe;

public class Katana implements Objet{

    //attributs de la classe Katana
    private int degats;

    private int posX,posY;

    private Aventurier av;

    private Labyrinthe lb;


    //constructeur du katana

    public Katana(Labyrinthe l, Aventurier a, int x, int y){

        this.lb = l;
        this.av =a;
        this.posX = x;
        this.posY = y;
        this.degats = 7 ;
    }

    //methode trancher, augmente les degats de l'aventuruer

    public void trancher(){

        if (this.av != null){

            this.av.setDegats(this.degats);
        }
    }

    public boolean etreRamasse(Aventurier av) {
        // TODO Auto-generated method stub
        return false;
    }

     //methode etreLache, anule les statistiques bonus et se separe de l'aventurier
    
    public boolean etreLache(){
        this.av.setDegats(this.av.getDegats()-this.degats);

        this.av = null;

        return true;
    }


    //getters

    public int getPosX(){

        return this.posX;
    }

    public int getPosY(){

        return this.posY;
    }

    public Labyrinthe getLb(){

        return this.lb;
    }
    public Aventurier getAv(){

        return this.av;
    }

    public int getDegats(){

        return this.degats;
    }

    public void setPosX(int x){
        this.posX = x;
    }

    public void setPosY(int y){
        this.posY = y;
    }

    @Override
    public void soigner() {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void hacher() {
        // TODO Auto-generated method stub
        
    }



}