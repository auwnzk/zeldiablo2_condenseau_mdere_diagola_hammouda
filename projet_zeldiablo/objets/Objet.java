package objets;

import zeldiablo.*;

public interface Objet{

    public void soigner();

    public void trancher();

    public void hacher();
    
    public boolean etreLache();

    public boolean etreRamasse(Aventurier av);

    public int getPosX();

    public int getPosY();

    public void setPosX(int x);

    public void setPosY(int y);

}