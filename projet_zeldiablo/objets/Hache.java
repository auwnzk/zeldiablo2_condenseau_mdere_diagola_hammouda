package objets;

import zeldiablo.*;

public class Hache implements Objet{

    //attributs de la classe Hache
    private int degats, pvSup;

    private int posX,posY;

    private Aventurier av;

    private Labyrinthe lb;


    //constructeur de la hache

    public Hache(Labyrinthe l, Aventurier a, int x, int y){

        this.lb = l;
        this.av = a;
        this.posX = x;
        this.posY = y;
        this.degats = 5;
        this.pvSup = 5;
    }

    //methode hacher, augmente les degats et pv max de l'aventurier

    public void trancher(){

        if (this.av != null){

            this.av.setDegats(this.degats);
            this.av.setPvMax(this.av.getPvMax()+this.pvSup);
        }
    }

    public boolean etreRamasse(Aventurier av) {
        // TODO Auto-generated method stub
        return false;
    }

    //methode lacherArme, anule les statistiques bonus et se separe de l'aventurier
    
    public boolean etreLache(){
        this.av.setDegats(this.av.getDegats()-this.degats);
        this.av.setPvMax(this.av.getPvMax()-this.pvSup);

        this.av = null; 
        return true;
    }


    //getters
    
    public int getPosX(){

        return this.posX;
    }

    public int getPosY(){

        return this.posY;
    }

    public Labyrinthe getLb(){

        return this.lb;
    }
    public Aventurier getAv(){

        return this.av;
    }

    public int getDegats(){

        return this.degats;
    }

    public void setPosX(int x){
        this.posX = x;
    }

    public void setPosY(int y){
        this.posY = y;
    }

    @Override
    public void soigner() {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void hacher() {
        // TODO Auto-generated method stub
        
    }

}