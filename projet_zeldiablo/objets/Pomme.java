package objets;

import zeldiablo.*;

public class Pomme implements Objet{

    //attributs de la classe Objet
    private int soin;

    private int posX,posY;

    private Aventurier av;

    private Labyrinthe lb;


    //constructeur de la pomme

    public Pomme(Labyrinthe l, Aventurier a, int x, int y){

        this.lb = l;
        this.av =a;
        this.posX = x;
        this.posY = y;
        this.soin = 15 ;
    }

    //soigne l'aventurier, ne peux pas se soigner au dessus de ses pv max

    public void Soigner(){

        this.av.setPv(this.av.getPv()+this.soin);
        if(this.av.getPv() > this.av.getPvMax()){
            this.av.setPv(50);
        }

        //apres consommation, la pomme n'appartient plus a l'aventurier
        this.av = null;
    }

    public boolean etreRamasse(Aventurier av) {
        // TODO Auto-generated method stub
        return false;
    }

    public boolean etreLache(){

        this.av = null;
        return true;
    }

    //getters

    public int getPosX(){

        return this.posX;
    }

    public int getPosY(){

        return this.posY;
    }

    public Labyrinthe getLb(){

        return this.lb;
    }
    public Aventurier getAv(){

        return this.av;
    }

    public int getSoin(){

        return this.soin;
    }

    public void setPosX(int x){
        this.posX = x;
    }

    public void setPosY(int y){
        this.posY = y;
    }

    @Override
    public void soigner() {
    }

    @Override
    public void trancher() {
    }

    @Override
    public void hacher() {   
    }
}