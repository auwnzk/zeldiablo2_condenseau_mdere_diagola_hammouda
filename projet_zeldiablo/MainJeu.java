import jeu.*;
import moteurJeu.*;

public class MainJeu {
    // InterruptedException peut venir de lancerJeu
    public static void main ( String [] args ) throws InterruptedException {
    // creation jeu et affichage
    ZeldiabloJeu jeu = new ZeldiabloJeu();
    ZeldiabloDessin zdd = new ZeldiabloDessin(jeu);
    // creation moteur de jeu
    MoteurGraphique moteur = new MoteurGraphique ( jeu , zdd );
    // lance le jeu qui tourne jusque la fin ( dans fenetre 400 x 400)
    moteur.lancerJeu(800 , 800);
    // lorsque le jeu est fini
    System.out.println (" Fin du Jeu " );
    }
}