package jeu;

import zeldiablo.*;
import moteurJeu.*;

// IMPORTS

public class ZeldiabloJeu implements Jeu{

    // private Labyrinthe labyrinthe;

    private Aventurier aventurier;
 
    public ZeldiabloJeu(){

        this.aventurier = new Aventurier(0,0);
    }

    public void evoluer(Commande c){

        aventurier.getLabyrinthe().evoluer();
        aventurier.evoluer(c);
        Entite[][] o = aventurier.getLabyrinthe().getEntites();
        for(int i=0;i<aventurier.getLabyrinthe().getTailleX();i++){
            for(int j=0;j<aventurier.getLabyrinthe().getTailleY();j++){
                if(o[i][j] != null){
                    o[i][j].evoluer(c); 
                }
            }
        }
    }

    public boolean etreFini(){
        return (aventurier.getPv() == 0);
    }

    public Aventurier getAventurier(){
        return this.aventurier;
    }

}