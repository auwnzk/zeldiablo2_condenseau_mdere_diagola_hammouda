package jeu;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import cases.*;
import zeldiablo.*;
import moteurJeu.*;

public class ZeldiabloDessin implements DessinJeu{

    private ZeldiabloJeu jeu;
	public static final int TAILLE = 30;
    public static final int TAILLEMUR = 30;

    public ZeldiabloDessin(ZeldiabloJeu j){

        this.jeu = j;
    }

    public void dessiner(BufferedImage image){
        Graphics2D g = (Graphics2D) image.getGraphics();
        Aventurier av = jeu.getAventurier();

        //dessine le labyrinthe
        Labyrinthe lb = av.getLabyrinthe();
        int posXLab = 0;
        int posYLab = 0;
        Case[][] c = lb.getScene(); 
            for(int i=0;i<lb.getTailleX();i++){
                for(int j=0;j<lb.getTailleY();j++){
                //Si la lettre est un W ,c'est a dire un mur
                if(c[i][j] instanceof Mur){
                    dessinerMur(g,posXLab,posYLab);
                }
                //Si la lettre est un V ,c'est a dire du vide
                else if(c[i][j] instanceof CaseVierge){
                    dessinerVoid(g,posXLab,posYLab);
                }else if(c[i][j] instanceof Piege){
                    dessinerPiege(g,posXLab,posYLab);
                }else{
                    dessinerError(g,posXLab,posYLab);
                }
                posXLab += TAILLEMUR;
                }
                //on revient a la ligne
                posXLab = 0;
                posYLab += TAILLEMUR;
            }
            //on met à jour le tableau

    	//dessine l'aventurier
		g.setColor(Color.blue);
		g.fillOval(av.getPosX() * TAILLE, av.getPosY() * TAILLE, TAILLE, TAILLE);

        //dessine les ennemis
        dessinerEntites(g,lb);
        //on dispose
        g.dispose();
}

    public void dessinerEntites(Graphics2D g,Labyrinthe lb){
        Entite[][] o = lb.getEntites();
        for(int i=0;i<lb.getTailleX();i++){
            for(int j=0;j<lb.getTailleY();j++){
                if(o[i][j] != null){
                    if(o[i][j] instanceof Monstre){
                        g.setColor(Color.red);
                        g.fillOval(j * TAILLE, i * TAILLE, TAILLE, TAILLE);
                    }else if(o[j][i] instanceof Aventurier){
                        g.setColor(Color.blue);
                        g.fillOval(j * TAILLE, i * TAILLE, TAILLE, TAILLE);
                    }
                }
            }
        }
    }

    public void dessinerPiege(Graphics2D g, int x, int y) {
        g.setColor(Color.orange);
        g.fillRect(x,y, TAILLEMUR, TAILLEMUR);
        g.setColor(Color.black);
        g.drawRect(x,y, TAILLEMUR, TAILLEMUR);
    }

    public void dessinerError(Graphics2D g, int x, int y) {
        g.setColor(Color.magenta);
        g.fillRect(x,y, TAILLEMUR, TAILLEMUR);
        g.setColor(Color.black);
        g.drawRect(x,y, TAILLEMUR, TAILLEMUR);
    }

    public void dessinerMur(Graphics2D g,int x, int y){
        g.setColor(Color.black);
        g.fillRect(x,y, TAILLEMUR, TAILLEMUR);
        g.setColor(Color.black);
        g.drawRect(x,y, TAILLEMUR, TAILLEMUR);
    }

    public void dessinerVoid(Graphics2D g,int x, int y){
        g.setColor(Color.lightGray);
        g.fillRect(x,y, TAILLEMUR, TAILLEMUR);
        g.setColor(Color.darkGray);
        g.drawRect(x,y, TAILLEMUR, TAILLEMUR);
    }


}