package test;

import junit.framework.TestCase;
import zeldiablo.*;
import objets.*;


public class TestInventaire{

  public void testConstructorInventaire(){
    Aventurier a = new Aventurier("Thomas");
    Inventaire i = a.getInv();
    assertEquals(3,i.length,"l'inventaire doit etre de taille 3");
    assertEquals(0,i.getNbObjets(),"l'inventaire doit etre vide a la creation");
  }

  public void testAjouterObjet(){
    Aventurier a = new Aventurier("Thomas");
    Inventaire i = a.getInv();
    Katana k = new Katana(a.getLabyrinthe(),a,0,0);
    boolean b = i.ajouterObjet(k);
    assertEquals(1,i.getNbObjets(),"il devrait y avoir 1 objet dans l'inventaire");
    assertEquals(k,i.getObjet(0),"le katana devrait se trouver a la position 0 de l'inventaire");
    assertEquals(true,b,"la methode devrait renvoyer true");
  }

  public void testAjouterObjetInventairePlein(){
    Aventurier a = new Aventurier("Thomas");
    Inventaire i = a.getInv();
    Katana k1 = new Katana(a.getLabyrinthe(),a,0,0);
    Katana k2 = new Katana(a.getLabyrinthe(),a,0,0);
    Katana k3 = new Katana(a.getLabyrinthe(),a,0,0);
    Katana k4 = new Katana(a.getLabyrinthe(),a,0,0);
    i.ajouterObjet(k1);
    i.ajouterObjet(k2);
    i.ajouterObjet(k3);
    boolean b = i.ajouterObjet(k4);
    assertEquals(3,i.getNbObjets(),"il devrait y avoir 3 objet dans l'inventaire");
    assertEquals(false,b,"la methode devrait renvoyer false");
  }

  public void testSuppObjet(){
    Aventurier a = new Aventurier("Thomas");
    Inventaire i = a.getInv();
    Katana k = new Katana(a.getLabyrinthe(),a,0,0);
    i.ajouterObjet(k);
    i.suppObjet(0);
    assertEquals(null,i.getObjet(0),"la place 0 de l'inventaire doit etre vide");
    assertEquals(0,i.getNbObjets(),"l'inventaire doit avoir 0 objets");
  }

  public void testSuppObjetNull(){
    Aventurier a = new Aventurier("Thomas");
    Inventaire i = a.getInv();
    Katana k = new Katana(a.getLabyrinthe(),a,0,0);
    i.ajouterObjet(k);
    i.suppObjet(1);
    assertEquals(null,i.getObjet(1),"la place 0 de l'inventaire doit rester vide");
    assertEquals(k,i.getObjet(0),"le katana doit rester a l'emplacement 0");
    assertEquals(1,i.getNbObjets(),"l'inventaire doit avoir 1 objet");
  }

  public void testSuppObjetInvVide(){
    Aventurier a = new Aventurier("Thomas");
    Inventaire i = a.getInv();
    i.suppObjet(0);
    assertEquals(0,i.getNbObjets(),"l'inventaire doit avoir 0 objet");
  }

}
