package test;

import junit.framework.TestCase;
import zeldiablo.Aventurier;

public class TestAventurier{

    //test de la methode SubirDegats
    public void testSubirDegats(){
      Aventurier a = new Aventurier("Thomas");
      a.subirDegats(4);
      a.subirDegats(10)
      assertEquals(46,a.getPv(),"le montant de pv n'est pas correspondant");
    }

    public void testSubirDegats2(){
      Aventurier a = new Aventurier("Thomas");
      a.testSubirDegats(53);
      assertEquals(0,a.getPv(),"l'aventurier ne peut pas avoir un montant de pv negatifs");
    }

    public void testRamasser(){
      Aventurier a = new Aventurier("Thomas");
      Objet k = new Katana(a.getLabyrinthe(),a,0,0);
      a.ramasser(k);
      assertEquals(k,a.getInv().getObjet(0),"l'aventurier devrait avoir un katana dans son inventaire");
      assertEquals(1,a.getInv().getNbObjets(),"l'aventurier a maintenant 1 objet dans son inventaire");
    }

    public void testLacher(){
      Aventurier a = new Aventurier("Thomas");
      Objet k = new Katana(a.getLabyrinthe(),a,0,0);
      a.ramasser(k);
      a.lacher(0);
      assertEquals(null,a.getInv().getObjet(0),"l'aventurier devrait ne plus avoir de katana dans son inventaire");
      assertEquals(0,a.getInv().getNbObjets(),"l'aventurier a maintenant 0 objet dans son inventaire");
    }

    public void testToString(){
      Aventurier a = new Aventurier("Thomas");
      String s1 = "Niveau : 1\ Points de vie : 50/50\n\n\n";
      String s2 = a.toString();
      assertEquals(s1,s2,"la methode toString est incorrecte");
    }

}
