package test;

import junit.framework.TestCase;
import zeldiablo.*;
public class TestLabyrinthe{

  //test du constructeur du labyrinthe
  public testLabyrinthe(){
    Aventurier a = new Aventurier(0,0);
    Labyrinthe lb = new Labyrinthe(1,a);
    assertEquals(10,lb.getTailleX(),"le niveau 1 doit avoir une taille de 10x10");
    assertEquals(10,lb.getTailleY(),"le niveau 1 doit avoir une taille de 10x10");
    lb = new Labyrinthe(2,a);
    assertEquals(20,lb.getTailleX(),"le niveau 2 doit avoir une taille de 20x20");
    assertEquals(20,lb.getTailleY(),"le niveau 2 doit avoir une taille de 20x20");
  }

  //test de la methode faireTableau
  public void testFaireTableau(){
    Aventurier a = new Aventurier(0,0);
    Labyrinthe lb = new Labyrinthe(1,a);
    lb.faireTableau();
    Labyrinthe lb2 = new Labyrinthe(2,a);
    lb2.faireTableau();
    assertEquals(false,lb.getLForm() == lb2.getLForm(),"le niveau 1 et 2 n'utilisent pas le meme fichier");
  }
  
}
