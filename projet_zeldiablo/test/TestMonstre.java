package test;

import junit.framework.TestCase;
import zeldiablo.*;

public class TestMonstre{

  //test de la methode estVivant
  public void testEstVivant{
  Monstre m = new Monstre(1,new Labyrinthe(new Aventurier())0,0);
    m.setPv(0);
    assertEquals(false,m.estVivant(),"le monstre doit etre mort");
    m.setPv(-8);
    assertEquals(false,m.estVivant(),"le monstre doit etre mort");
  }

  //test de la methode attaquer
  public void testAttaquer(){
    Aventurier a = new Aventurier(0,0);
    Labyrinthe lb = a.getLabyrinthe();
    Monstre m = new Monstre(lb,0,0);
    m.attaquer();
    assertEquals(35,a.getPv(),"l'aventurier doit avoir perdu 35pv");
  }
}
