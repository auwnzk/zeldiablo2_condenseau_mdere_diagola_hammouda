package cases;

import zeldiablo.Aventurier;

public class Mur implements Case {

  private int x;
  private int y;

  public Mur(int x,int y){
    this.x = x;
    this.y = y;
  }

@Override
public void activation(Aventurier a) {
    // TODO Auto-generated method stub
    
}

@Override
public int getPosX() {
  return this.x;
}

@Override
public int getPosY() {
  return this.y;
}
}