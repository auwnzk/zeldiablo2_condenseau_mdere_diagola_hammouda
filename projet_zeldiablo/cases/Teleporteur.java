package cases;

import zeldiablo.*;

public class Teleporteur extends CaseVide{
    private int newPosX;
    private int newPosY;

    public Teleporteur(int x, int y, int newX, int newY){
        super(x,y);
        this.newPosX = newX;
        this.newPosY = newY;

    }

    public void activation(Aventurier a){
        if(a.getPosX() == this.getPosX() && a.getPosY() == this.getPosY()){
            a.setPosX(newPosX);
            a.setPosY(newPosY);
        }
    }
}