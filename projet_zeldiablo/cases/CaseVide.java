package cases;

import zeldiablo.*;

public abstract class CaseVide implements Case{

    private int x;
    private int y;

    public CaseVide(int x,int y){
        this.x=x;
        this.y=y;
    }

    public int getPosX(){
        return this.x;
    }
    public int getPosY(){
        return this.y;
    }
    
    public abstract void activation(Aventurier a);
}