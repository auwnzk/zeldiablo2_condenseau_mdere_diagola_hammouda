package cases;

import zeldiablo.Aventurier;
import zeldiablo.Labyrinthe;

public class Piege extends CaseVide{
    private int degats;
    private Labyrinthe lb;

    public Piege(int x,int y){
        super(x,y);
        this.degats = 1;
    }

    public void activation(Aventurier a){
        if(a.getPosX() == this.getPosX() && a.getPosY() == this.getPosY() ){

            lb.getAventurier().subirDegats(this.degats);
            
        }
    }
}