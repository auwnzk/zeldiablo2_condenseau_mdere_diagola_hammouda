package zeldiablo;

import objets.Objet;

public class Inventaire {

    //tableau regroupant les objets en inventaire, l'aventurier peut stocker 3 objets maximum 
    private Objet[] tabObjet;

    //indique le nombre d'objets dans l'inventaire
    private int nbObjets;


    //constructeur de l'inventaire
    public Inventaire(){
        this.tabObjet = new Objet[2];
        this.nbObjets = 0;
    }

    //methode qui permet d'ajouter un objet dans l'inventaire
    public boolean ajouterObjet(Objet o){
        if(this.nbObjets >= 3){
            return  false;
        }else{
            this.tabObjet[this.nbObjets] = o;
            this.nbObjets += 1;
            return true;
        }
    }

    /** methode qui permet d'enlever un objet de l'inventaire
    * la parametre "main" indique l'objet que l'aventurier a dans la main
    * actuellement
    */
    public boolean suppObjet(int main){
        if(this.nbObjets == 0){
            return false;
        }else{
          this.tabObjet[main] = null;
          this.nbObjets -= 1;
          return true;
        }
    }

    public Objet[] getTabObjet(){
        return this.tabObjet;
    }

    public Objet getObjet(int i){
        return this.tabObjet[i];
    }

    public int getNbObjets(){
        return this.nbObjets;
    }


}