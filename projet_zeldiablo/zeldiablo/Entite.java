package zeldiablo;

import moteurJeu.Commande;

public abstract class Entite {

    private int posX,posY;

    public Entite(int x, int y) {
        this.posX = x;
        this.posY = y;
    }

    public boolean estAdjacent(Entite e){
        boolean b = false;
        //on regarde si l'entite est au dessus de l'autre
        if((e.getPosY() == this.getPosY()-1) && (e.getPosX() == this.getPosX())){
            b=true;
        }
        //on regarde si l'entite est en dessous de l'autre
        if((e.getPosY() == this.getPosY()+1) && (e.getPosX() == this.getPosX())){
            b=true;
        }
        //on regarde si l'entite est a gauche de l'autre
        if((e.getPosX() == this.getPosX()-1) && (e.getPosY() == this.getPosY())){
            b=true;
        }
        //on regarde si l'entite est a droite de l'autre
        if((e.getPosX() == this.getPosX()+1) && (e.getPosY() == this.getPosY())){
            b=true;
        }
        return b;
    }

    public abstract boolean estVivant();
    public abstract void evoluer(Commande c);
    
    public int getPosX(){
        return this.posX;
    }
    public int getPosY(){
        return this.posY;
    }
    public void setPosX(int x){
        this.posX = x;
    }
    public void setPosY(int y){
        this.posY = y;
    }

}
