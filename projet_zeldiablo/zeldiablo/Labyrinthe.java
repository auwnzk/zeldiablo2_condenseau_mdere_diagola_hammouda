package zeldiablo;

import cases.*;
import java.io.*;

public class Labyrinthe {

//attributs de notre labyrinthe

    private int niveau;
    private int tailleX;
    private int tailleY;
    private Aventurier av;
    private Case[][] scene;
    private File lForm;
    private Entite[][] entites;

// constructeur qui crée un labyrinthe de differentes taille en fonction du niveau

    public Labyrinthe(int n, Aventurier av){
        this.av = av;
        this.niveau = n;
        switch(n){
            case 1:
                this.tailleX = 10;
                this.tailleY = 10;
                this.entites = new Entite[10][10];
                this.entites[5][5] = new Monstre(this,5,5);
                this.lForm = new File("niveaux/niveau1.txt");
                break;

            case 2:
                this.tailleX = 20;
                this.tailleY = 20;
                this.entites = new Entite[20][20];
                this.entites[9][3] = new Monstre(this,9,3);
                this.entites[5][8] = new Monstre(this,5,8);
                this.entites[12][12] = new Monstre(this,12,12);
                this.lForm = new File("niveaux/niveau2.txt");
                break;

            case 3:
                this.tailleX = 20;
                this.tailleY = 20;
                this.entites = new Entite[20][20];
                lForm = new File("niveaux/niveau3.txt");
                break;
        }
        faireTableau();
    }

    public void faireTableau(){
        try{
            this.scene = new Case[tailleX][tailleY];
            // Le fichier d'entrée
            File file = this.lForm;
            // Créer l'objet File Reader
            FileReader fr = new FileReader(file);  
            // Créer l'objet BufferedReader        
            BufferedReader br = new BufferedReader(fr);  
            //StringBuffer sb = new StringBuffer();    
            int c;
            for(int i=0;i<this.tailleX;i++){
                c = br.read();
                for(int j=0;j<this.tailleY;j++){
                //Si la lettre est un W ,c'est a dire un mur
                if((char)c == 'W'){
                    this.scene[i][j] = new Mur(i,j);
                }
                //Si la lettre est un V ,c'est a dire du vide
                else if((char)c == 'V'){
                    this.scene[i][j] = new CaseVierge(i,j);
                //Si la lettre est un T ,c'est a dire un piege
                }else if((char)c == 'T'){
                    this.scene[i][j] = new Piege(i,j);
                }else{
                    this.scene[i][j] = null;
                }
                c = br.read();
                }
                br.read();
            }
            fr.close();
        } catch(IOException e){
            e.printStackTrace();
        }
    }

    public boolean estAccessible(int x,int y){
        Case[][] c = this.scene;
        boolean b;
        if(x<0 || y<0 || x>=this.tailleX || y>=this.tailleY){
            b=false;
        }else if (c[y][x] instanceof Mur){
            b=false;
        }else{
            b=true;
        }
        return b;
    }

    public void evoluer(){
        
        faireTableau();
    }

// getters et setters de la classe Labyrinthe

    public int getNiveau(){
        return (this.niveau);
    }


    public int getTailleX(){
        return (this.tailleX);
    }

    public int getTailleY(){
        return (this.tailleY);
    }

    public Aventurier getAventurier(){
        return this.av;
    }

    public File getLForm(){
        return this.lForm;
    }

    public Case[][] getScene(){
        return this.scene;
    }

    public Entite[][] getEntites(){
        return this.entites;
    }


    public void setTailleX(int Tx){
        if (Tx>0){
            this.tailleX=Tx;
        }
    }

    public void setTailleY(int Ty){
        if (Ty>0){
            this.tailleY=Ty;
        }
    }

}