package zeldiablo;

import moteurJeu.Commande;

public class Monstre extends Entite{


//attributs de notre Opps

    private int pv;
    private int degats;
    private Labyrinthe lb;
    private int posX,posY;

// constructeur qui crée un Opps

    public Monstre(Labyrinthe lb,int x, int y) {
        super(x,y);
        this.degats= 5;
        this.pv= 15;
        this.lb = lb;
    }

    public boolean estVivant(){
        if(this.pv <= 0){
            return false;
        }else{
            return true;
        }
    }

    public void evoluer(Commande c){
        attaquer();
    }

    public void attaquer(){
        Aventurier a = this.gLabyrinthe().getAventurier();
        if((estVivant())&&(this.estAdjacent(a))){
            a.subirDegats(0);
        }
    }

// getteurs et setteurs de la classe Opps

    public int getPv(){
        return (this.pv);
    }

    public void setPv(int pv) {
        this.pv = pv;
    }

    public int getDegats() {
        return degats;
    }

    public void setDegats(int degats) {
        this.degats = degats;
    }

    public int getPosX(){
        return this.posX;
    }
    public int getPosY(){
        return this.posY;
    }
    public Labyrinthe gLabyrinthe(){
        return this.lb;
    }
    public void setPosX(int x){
        this.posX = x;
    }
    public void setPosY(int y){
        this.posY = y;
    }


}