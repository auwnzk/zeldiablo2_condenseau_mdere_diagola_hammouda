package zeldiablo;

import moteurJeu.*;
import objets.*;

public class Aventurier extends Entite{

    //attributs de la classe Aventurier

    private int posY , posX;
    private Labyrinthe labyrinthe;
    private Inventaire inv;
    private int pv, pvMax;
    private int degats;

    //l'attribut main designe l'objet que l'aventurier a en main, selon sa position dans l'inventaire
    // private int main;

    //constructeur de l'aventurier
    public Aventurier(int x,int y){
        super(x,y);
        this.labyrinthe = new Labyrinthe(1,this);
        this.inv = new Inventaire();
        this.pv = 50;
        this.pvMax = 50;
        this.degats = 1;
        //this.main = 0;

        /** on initialise la position de depart de l'aventurier
        l'attribut posX sera toujours a 0, mais l'attribut posY
        varie en fonction du niveau du labyrinthe
        */
        switch(this.labyrinthe.getNiveau()){
            case 1:
                this.posX = 0 ;
                this.posY = 1 ;
                break;
            case 2:
                this.posX = 2 ;
                this.posY = 0 ;
                break;
            case 3:
                this.posY = 0 ;
                break;
            default:
                this.posY= 0 ;
        }
    }

    public boolean attaquer(){
        return false;
    }

    public void subirDegats(int k){
        this.pv -= k;
        if(this.pv<0){
            this.pv = 0;
        }
    }

    public boolean estVivant(){
        return this.pv == 0;
    }

    public boolean ramasser(Objet o){
        if(o.getPosX() == this.getPosX() && (o.getPosY() == this.getPosY()) ){
            o.etreRamasse(this);
            inv.ajouterObjet(o);
        }
        return true;
    }

    public boolean lacher(int main){
        Objet obj = inv.getTabObjet()[main];
        obj.etreLache();
        inv.suppObjet(main);

        return true;

    }

    public void deplacer(Commande c){
        if (c.haut){
            if(this.labyrinthe.estAccessible(this.getPosX(),this.getPosY()-1)){
                this.posY --;
            }
        }else if (c.bas && this.labyrinthe.estAccessible(this.getPosX(), this.getPosY()+1)){
            this.posY ++;
        }else if (c.gauche && this.labyrinthe.estAccessible(this.getPosX()-1, this.getPosY()) ){
            this.posX --;
        }else if (c.droite && this.labyrinthe.estAccessible(this.getPosX()+1, this.getPosY())){
            this.posX ++;
        }
        //
    }

    public void changerObjet(){
        // A COMPLETER
    }

    public String toString(){
        String s = ("Niveau : "+ this.getLabyrinthe().getNiveau());
        s = s + ("\n Points de Vie : " + this.getPv() + "/" + this.getPvMax() + "\n\n\n");

        // s = s + ("Inventaire : " + this.getInv().toString() );

        return s;
    }

    public void evoluer(Commande c){
        deplacer(c);
        changerNiveau(c);
    }

    public void changerNiveau(Commande c){
        //niveau 1 au niveau 2
        if((this.labyrinthe.getNiveau() == 1) && (this.getPosX()== 8) && (this.getPosY() == 9) && c.bas){
            this.setPosX(2);
            this.setPosY(0);
            this.labyrinthe = new Labyrinthe(2,this);
        }
        //niveau 2 au niveau 1
        if((this.labyrinthe.getNiveau() == 2) && (this.getPosX()== 2) && (this.getPosY() == 0) && c.haut){
            this.setPosX(8);
            this.setPosY(9);
            this.labyrinthe = new Labyrinthe(1,this);
        }
        //niveau 2 au niveau 3
        if((this.labyrinthe.getNiveau() == 2) && (this.getPosX()== 14) && (this.getPosY() == 0) && c.haut){
            this.labyrinthe = new Labyrinthe(3,this);
            this.setPosX(6);
            this.setPosY(19);
        }
        //niveau 3 au niveau 2
        if((this.labyrinthe.getNiveau() == 3) && (this.getPosX()== 6) && (this.getPosY() == 19) && c.bas){
            this.setPosX(14);
            this.setPosY(0);
            this.labyrinthe = new Labyrinthe(2,this);
        }
    }

    //getters
    public int getPosX(){
        return this.posX;
    }

    public int getPosY(){
        return this.posY;
    }

    public Labyrinthe getLabyrinthe(){
        return this.labyrinthe;
    }

    public Inventaire getInv(){
        return this.inv;
    }

    public int getPv(){
        return this.pv;
    }

    public int getPvMax(){
        return this.pvMax;
    }

    public int getDegats(){
        return this.degats;
    }

    //setters

    public void setPosX(int x){
        this.posX = x;
    }

    public void setPosY(int y){
        this.posY = y;
    }

    public void setPv(int p){
        this.pv = p;
    }

    public void setPvMax(int p){
        this.pvMax = p;
    }

    public void setDegats(int d){
        this.degats = d;
    }

}
